import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sportsdb/browser/browser.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sportsdb/model/sports.dart';
//import 'package:sportsdb/search/search.dart';

class League extends StatefulWidget {
  final String country;
  League({Key key, this.country}) : super(key: key);

  @override
  _LeagueState createState() => new _LeagueState();
}

class _LeagueState extends State<League> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  //String url = "https://www.google.com";
  Future<List<Sports>> allLeague;
  List<Sports> leagues = [];
  List<Sports> filteredLeagues = [];
  String apiUrl;
  bool loading = true;

  Future<List<Sports>> fetchLeagues() async {
    var listLeague = List<Sports>();
    var response = await http.get(
        apiUrl
    );
    //print(response.body);
    if(response.statusCode == 200){
      var jsonData = json.decode(response.body)['countrys'];
      for(var s in jsonData){
        Sports sport = Sports(s['strLeague'],s['strLogo'],s['strFacebook'],s['strTwitter'], s['strSport']);
        listLeague.add(sport);
      }
      //print(leagues.length);
      return listLeague;
    } else {
      throw Exception('Failed to load');
    }
  }

  @override
  void initState() {
    apiUrl = "https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=${widget.country}";
    //allLeague = fetchLeagues();
    super.initState();
    fetchLeagues().then((value) {
      setState(() {
        leagues.addAll(value);
        filteredLeagues = leagues;
        loading = false;
      });
    });
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
            border: OutlineInputBorder(
                borderRadius: BorderRadius.all(
                    Radius.circular(30.0)
                ),
                borderSide: BorderSide.none
            ),
            contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
            filled: true,
            hintText: 'Search Leagues',
            hintStyle: TextStyle(color: Colors.black)
        ),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            filteredLeagues = leagues.where((league) {
              var leagueTitle = league.strLeague.toLowerCase();
              return leagueTitle.contains(text);
            }).toList();
          });
        },
      ),
    );
  }

  _itemList(index){
    return loading ? Center(
      child: CircularProgressIndicator(),
    ) : Card(
      child: ListTile(
        title: Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Text(
            filteredLeagues[index].strLeague,
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
        subtitle: Row(
          children: <Widget>[
            FlatButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WebView(
                      url: "https://${filteredLeagues[index].strTwitter}"
                  )),
                );
              },
              child: Image(
                image: AssetImage('assets/icons/twitter.png'),
                width: 25.0,
                height: 25.0,
              ),
            ),
            FlatButton(
              onPressed: (){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WebView(
                      url: "https://${filteredLeagues[index].strFacebook}"
                  )),
                );
              },
              child: Image(
                image: AssetImage('assets/icons/facebook.png'),
                width: 25.0,
                height: 25.0,
              ),
            )
          ],
        ),
        //trailing: Icon(Icons.more_vert),
        //isThreeLine: true,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        //elevation: 0.0,
        title: Text(
          widget.country,
          style: Theme.of(context).textTheme.headline2,
        ),
      ),
      body: Container(
          padding: EdgeInsets.all(12.0),
          child: ListView.builder(
            shrinkWrap: true,
            itemCount: filteredLeagues.length+1,
            itemBuilder: (context, int index){
              return index == 0 ? _searchBar() : _itemList(index-1);
            },
          ),
      ),
    );
  }
}