import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebView extends StatefulWidget {
  final String url;
  WebView({Key key, this.url}) : super(key: key);

  @override
  _WebViewState createState() => _WebViewState();
}

class _WebViewState extends State<WebView> {

  TextEditingController controller = TextEditingController();
  FlutterWebviewPlugin flutterWebviewPlugin = FlutterWebviewPlugin();
  bool _isLoading = false;

  launchUrl() {
    setState(() {
      flutterWebviewPlugin.reloadUrl(widget.url);
    });
  }

  @override
  void initState() {
    super.initState();
    flutterWebviewPlugin.onStateChanged.listen((WebViewStateChanged wvs) {
      if(wvs.type == WebViewState.startLoad){
        setState(() {
          _isLoading = true;
        });
      }
      if(wvs.type == WebViewState.finishLoad){
        setState(() {
          _isLoading = false;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget leading;
    if (_isLoading) {
      leading = new CircularProgressIndicator(strokeWidth: 1.0);
    }
    return WebviewScaffold(
      appBar: AppBar(
        title: Text(
          'Browser'
        ),
        leading: leading,
      ),
      url: widget.url,
      withZoom: false,
      withJavascript: true,
      withLocalStorage: true,
    );
  }
}