import 'package:flutter/material.dart';
import 'package:sportsdb/landing/home.dart';
import 'package:sportsdb/search/search.dart';
import 'package:sportsdb/all_sports/league.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Sports DB',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          brightness: Brightness.light,
          primaryColor: Colors.redAccent,
          //accentColor: Colors.cyan[600],
          fontFamily: 'Baloo2',
          textTheme: TextTheme(
            headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
            headline2: TextStyle(color: Colors.white, fontSize: 36.0, fontWeight: FontWeight.bold),
            headline3: TextStyle(color: Colors.black, fontSize: 24.0, fontWeight: FontWeight.bold),
            bodyText1: TextStyle(fontSize: 20.0, fontFamily: 'Baloo2'),
          ),
        ),
        home: Home()
      //home: Search(),
      //home: League(),
    );
  }
}