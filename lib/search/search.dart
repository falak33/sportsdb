import 'package:flutter/material.dart';
import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sportsdb/model/sports.dart';

class Search extends StatefulWidget {
  final String country;
  Search({Key key, this.country}) : super(key: key);

  @override
  _SearchState createState() => new _SearchState();
}

class _SearchState extends State<Search> {
  GlobalKey<AutoCompleteTextFieldState<Sports>> key = new GlobalKey();
  AutoCompleteTextField searchTextField;
  TextEditingController controller = new TextEditingController();
  List<Sports> sports = [];
  String url;


  Future<void> fetchSports() async {
    var response = await http.get(
        url
    );
    //print(response.body);
    if(response.statusCode == 200){
      var jsonData = json.decode(response.body)['countrys'];
      for(var s in jsonData){
        Sports sport = Sports(s['strLeague'],s['strLogo'],s['strFacebook'],s['strTwitter'], s['strSport']);
        sports.add(sport);
      }
      //print(sports.length);
      //return sports;
    } else {
      throw Exception('Failed to load');
    }
  }

  @override
  void initState() {
    url = "https://www.thesportsdb.com/api/v1/json/1/search_all_leagues.php?c=${widget.country}";
    fetchSports();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
          child: Container(
              padding: EdgeInsets.all(18.0),
              child: Column(
                children: <Widget>[
                  searchTextField = AutoCompleteTextField<Sports>(
                      style: TextStyle(color: Colors.black, fontSize: 16.0),
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(30.0)
                              ),
                              borderSide: BorderSide.none
                          ),
                          contentPadding: EdgeInsets.fromLTRB(10.0, 15.0, 10.0, 15.0),
                          filled: true,
                          hintText: 'Search Leagues',
                          hintStyle: TextStyle(color: Colors.black)
                      ),
                      itemSubmitted: (item) {
                        setState(() {
                          searchTextField.textField.controller.text = item.strLeague;
                        });
                      },
                      clearOnSubmit: false,
                      key: key,
                      suggestions: sports,
                      itemBuilder: (context, item) {
                        return Text(
                          item.strLeague,
                          style: TextStyle(
                              fontSize: 16.0
                          ),
                        );
                      },
                      itemSorter: (a, b) {
                        return a.strLeague.compareTo(b.strLeague);
                      },
                      itemFilter: (item, query) {
                        return item.strLeague
                            .toLowerCase()
                            .startsWith(query.toLowerCase());
                      }
                  ),
                ],
              )
          )
      ),
    );
  }
}