class Sports{
  final String strLeague;
  final String strLogo;
  final String strFacebook;
  final String strTwitter;
  final String strSport;

  Sports(
      this.strLeague,
      this.strLogo,
      this.strFacebook,
      this.strTwitter,
      this.strSport
  );
}